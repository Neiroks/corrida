$(document).ready(function() {

$('.navbar-toggler').click(function(){
  $(this).next('.navbar_wrap').slideToggle();
  return false;
})
  new Swiper(".swiper-container1", {
    slidesPerView: 1,
    spaceBetween: 0,
    loop: !0,
    pagination: {
      el: ".swiper-pagination",
      clickable: !0
    },
    navigation: {
      nextEl: ".swiper-button-next1",
      prevEl: ".swiper-button-prev1"
    }
  });
  // new Swiper('.object__partners-slider', {
  //     slidesPerView: 5,
  //     spaceBetween: 40,
  //     navigation: {
  //         nextEl: ".swiper-button-next2",
  //         prevEl: ".swiper-button-prev2"
  //       },
  //     loop: !0,
  // });
  new Swiper('.object__partners-slider', {
      slidesPerView: 4,
      spaceBetween: 40,
      navigation: {
          nextEl: ".swiper-button-next2",
          prevEl: ".swiper-button-prev2"
        },
      loop: !0,
      breakpoints: {
        // when window width is >= 320px
        320: {
          slidesPerView: 1,
          spaceBetween: 20
        },
        // when window width is >= 480px
        480: {
          slidesPerView: 1,
          spaceBetween: 30
        },
        // when window width is >= 640px
        640: {
          slidesPerView: 2,
          spaceBetween: 40
        },
        991: {
          slidesPerView: 3,
          spaceBetween: 40
        }
      }
  });
    new Swiper('.singleObject__gallery', {
        slidesPerView: 1,
        spaceBetween: 0,
        loop: !0,
        navigation: {
          nextEl: ".swiper-button-next3",
          prevEl: ".swiper-button-prev3"
        }
    });
  
  $(".tab-content>div").not(":first-of-type").hide(), $(".tab-menu li").each(function(e) {
      $(this).attr("data-tab", "tab" + e)
    }), $(".tab-content>div").each(function(e) {
      $(this).attr("data-tab", "tab" + e)
    }), $(".tab-menu li").on("click", function() {
      var e = $(this).data("tab"),
        t = $(this).closest(".tab-wrapper");
      t.find(".tab-menu li").removeClass("active"), $(this).addClass("active"), t.find(".tab-content>div").hide(), t.find(".tab-content>div[data-tab=" + e + "]").show()
    }), $(window).on("scroll", function() {
      $(window).scrollTop() >= 400 ? $(".up").fadeIn() : $(".up").fadeOut()
    }), $(".up").on("click", function() {
      $("html,body").animate({
        scrollTop: 0
      }, 1e3)
    }),
    $(".scrollTo a").click(function(e) {
      e.preventDefault();
      var t = $(this).attr("href");
      return $("html, body").animate({
        scrollTop: $(t).offset().top
      }, 800), !1
    }),
    $(".showModal").on("click", function(e) {
      e.preventDefault(), $(".overlay,.modal__recall").fadeIn()
    }), $(".closeModal").on("click", function() {
      $(this).parent().fadeOut(), $(".overlay").fadeOut()
    }), 
    // $("#menu").mmenu({
    //   extensions: ["fx-menu-zoom", "pagedim-black", "theme-dark"],
    //   counters: !0
    // }),
    $(".newsPage__item").slice(0, 6).show(), $(".newsPage__showMore").on("click", function(e) {
      e.preventDefault(), $(".newsPage__item:hidden").slice(0, 6).fadeIn()
    }), $(".objectItem").slice(0, 3).show(), $(".object__showMore").on("click", function(e) {
      e.preventDefault(), $(".objectItem:hidden").slice(0, 3).fadeIn()
    }), $(".getInfo__file").styler();
  $('.services__menu1 a').on('click', function(e) {
    e.preventDefault();
    var bgLink = $(this).attr('href');
    var servLink = $(this).attr('data-link');
    $('a#service_link').css('display', 'flex');
    $('a#service_link').attr('href', servLink);
    $('#services').css({
      "background": "url(" + bgLink + ") no-repeat center / cover"
    });
  });
  $('.services__menu2 a').on('click', function(e) {
    e.preventDefault();
    var bgLink = $(this).attr('href');
    $('.sphere').css({
      "background": "url(" + bgLink + ") no-repeat center / cover"
    });
  });
  $('.services__menu3 a').on('click', function(e) {
    e.preventDefault();
    var bgLink = $(this).attr('href');
    $('#object').css({
      "background": "url(" + bgLink + ") no-repeat center / cover"
    });
  });
  
  // customize AjaxForm
    // $(document).ready(function(){
    //     AjaxForm.Message.success = 
    // 	function(message, sticky) {
    // 		if (message) {
    // 			$.jGrowl(message, {theme: 'taleby-message-success', sticky: true});
    // 		}
    // 	}; 
    // });
  //Аккордеон
//   $(function() {
//   var Accordion = function(el, multiple) {
//     this.el = el || {};
//     // more then one submenu open?
//     this.multiple = multiple || false;
    
//     var dropdownlink = this.el.find('.accordion__item-link');
//     dropdownlink.on('click',
//                     { el: this.el, multiple: this.multiple },
//                     this.dropdown);
//   };
  
//   Accordion.prototype.dropdown = function(e) {
//     var $el = e.data.el,
//         $this = $(this),
//         //this is the ul.submenuItems
//         $next = $this.next();
    
//     $next.slideToggle();
//     $this.parent().toggleClass('open');
    
//     if(!e.data.multiple) {
//       //show only one menu at the same time
//       $el.find('.submenuItems').not($next).slideUp().parent().removeClass('open');
//     }
//   };
  
//   var accordion = new Accordion($('.accordion-wrp'), false);
// });
$(function() {
    var price_name = document.querySelectorAll('.table_desc_name');
    for (var i = 0; i < price_name.length; i++) {
    		price_name[i].onclick = function () {
    		this.nextElementSibling.classList.toggle('price_list_desc_name');
    	}
    }
    var img_plus = document.querySelectorAll('.plus_price');
    for (var i = 0; i < img_plus.length; i++) {
    		img_plus[i].onclick = function () {
    		this.nextElementSibling.classList.toggle('price_list_desc_name');
    	}
    }
});
    //faq accordion
    $('.faq__desc').hide();
    $('.faq__name').on('click', function(){
        $(this).siblings('.faq__desc').slideToggle();
        $(this).toggleClass('active');
    })
});
(function () {
    $('.phone-mask').mask('+7(999) 999-99-99');
})();

var slider = $("#amount-one");
var output = $("#demo");
function calc() {
  var sliderVal = slider.val();
  var month = $(".term-investment__button.active").data("value");
  var payment = $("#result");
  output.val(sliderVal); 
  var rate = 42.72;
  var k = rate / 12 / 100;

  var result = Math.floor(sliderVal * (k * (Math.pow(1 + k, month))) / ((Math.pow(1 + k, month)) - 1));
  payment.html(result.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
  
}
calc();

// slider.change(function(){
//   calc();
// });

slider.on('input', function(){
  calc();
});

output.change(function(){
  var val = parseInt($(this).val());
  slider.val(val);
  calc();
});

$('.term-investment__button').click(function(){
  $('.term-investment__button').removeClass("active");
  $(this).addClass("active");
  if($(this).data("value")=='24'){
    rate = 42.72;
  }else if($(this).data("value")=='36'){
    rate = 51.1;
  }else{
    rate = 62.26;
  }
  
  calc();
});

var $range = $("#amount-one");
var $input = $("#demo");
var instance;
var min = 50000;
var max = 1000000;

$range.ionRangeSlider({
    skin: "round",
    type: "single",
    min: min,
    max: max,
    from: 50000,
    onStart: function(data) {
        $input.prop("value", data.from);
    },
    onChange: function(data) {
        $input.prop("value", data.from);
    }
});

instance = $range.data("ionRangeSlider");

$input.on("input", function() {
    var val = $(this).prop("value");

    // validate
    if (val < min) {
        val = min;
    } else if (val > max) {
        val = max;
    }

    instance.update({
        from: val
    });
});

