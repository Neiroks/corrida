$(document).ready(function () {

  if ($('.gallery-slider').length) {
    const slider = new ChiefSlider('.gallery-slider', {
      loop: false
    });
  }

  if ($('.gallery-slider-mobile').length) {
    const slider = new ChiefSlider('.gallery-slider-mobile', {
      loop: false
    });
  }


  $('.navbar-toggler').click(function () {
    if ($('.navbar-toggler').hasClass("burger-opened")) {
      $('.navbar-toggler').removeClass("burger-opened");
      $('body').css('overflow', 'auto');
    } else {
      $('.navbar-toggler').addClass("burger-opened");
      $('body').css('overflow', 'hidden');

    }

    if ($('.navbar_wrap').hasClass("active")) {
      $('.navbar_wrap').removeClass("active");
    } else {
      $('.navbar_wrap').addClass("active");
    }
    return false;
  })

  $('.calculator-switchers').on('click', '.calculator-switcher-item', (e) => {
    const $this = e.currentTarget;
    $('.calculator-switchers').find('.calculator-switcher-item').removeClass('active');
    $this.classList.toggle('active');

    if ($this.dataset['tab'] === '1') {
      $('body').find('.first-calculator').css('display', 'block')
      $('body').find('.second-calculator').css('display', 'none')
    } else if ($this.dataset['tab'] === '2') {
      $('body').find('.second-calculator').css('display', 'block')
      $('body').find('.first-calculator').css('display', 'none')
    }

  })

  // new Swiper(".swiper-container1", {
  //   slidesPerView: 1,
  //   spaceBetween: 0,
  //   loop: !0,
  //   pagination: {
  //     el: ".swiper-pagination",
  //     clickable: !0
  //   },
  //   navigation: {
  //     nextEl: ".swiper-button-next1",
  //     prevEl: ".swiper-button-prev1"
  //   }
  // });

  // new Swiper('.singleObject__gallery', {
  //     slidesPerView: 1,
  //     spaceBetween: 0,
  //     loop: !0,
  //     navigation: {
  //       nextEl: ".swiper-button-next3",
  //       prevEl: ".swiper-button-prev3"
  //     }
  // });


  // $(".scrollTo a").click(function(e) {
  //   e.preventDefault();
  //   var t = $(this).attr("href");
  //   return $("html, body").animate({
  //     scrollTop: $(t).offset().top
  //   }, 800), !1
  // }),
  $(".showModal").on("click", function (e) {
    e.preventDefault(), $(".overlay,#callback").fadeIn()
  }), 
  $("#callback .closeModal").on("click", function () {
    $(this).parent().fadeOut(), $(".overlay").fadeOut()
  }),
    $(".showModalOrder").on("click", function (e) {
      e.preventDefault(), $(".overlay,#modal_order").fadeIn()
    }), 
    $("#modal_order .closeModal").on("click", function () {
      $(this).parent().fadeOut(), $(".overlay").fadeOut()
    }),
    $(".showModalOrderSecondCalc").on("click", function (e) {
      e.preventDefault(), $(".overlay,#modal_order_second").fadeIn()
    }), $("#modal_order_second .closeModal").on("click", function () {
      $(this).parent().fadeOut(), $(".overlay").fadeOut()
    }), $("#modal_order .closeModal").on("click", function () {
      $(this).parent().fadeOut(), $(".overlay").fadeOut()
    }),
    $(".showQiwiModal").on("click", function(e) {
      e.preventDefault(), $(".overlay,#callback_qiwi_modal").fadeIn()
    }),
    $("#callback_qiwi_modal .closeModal").on("click", function(e) {
      $(this).parent().fadeOut(), $(".overlay").fadeOut()
    });

    $('form.pay-form').on('submit', function(e) {
      e.preventDefault();
      var form = $(this);
      var formData = form.serializeArray();
      let data = `&comment=${formData[0].value},%0A${formData[1].value}${formData[3].value == '' ? '' : `,%0A${formData[3].value}`}`;
      $(formData).each((i,elem)=>{
        if(elem.name == 'extras[cf1]' || elem.name == 'extras[cf2]' || elem.name == 'comment'){
          return;
        }
        data += `&${elem.name}=${encodeURIComponent(elem.value)}`
      });
      
      var qiwiLink = 'https://oplata.qiwi.com/create?publicKey=5nAq6abtyCz4tcDj89e5w7Y5i524LAFmzrsN6bQTQ3ceEvMvCq55ToeErzhvKjHQyaR9AkzHWu85tcLZaVenBjGeN93qGQd4arwaqKZSKAMMuuA1Jcq1Yay4qi&customFields[themeCode]=autocorrida1' + data;
     /*  window.location.href = qiwiLink; */
     console.log(data);
     console.log(qiwiLink)
     /* console.log('https://oplata.qiwi.com/create?publicKey=5nAq6abtyCz4tcDj89e5w7Y5i524LAFmzrsN6bQTQ3ceEvMvCq55ToeErzhvKjHQyaR9AkzHWu85tcLZaVenBjGeN93qGQd4arwaqKZSKAMMuuA1Jcq1Yay4qi' + data) */
    });
    
});

//faq accordion
// $('.faq__desc').hide();
// $('.faq__name').on('click', function(){
//     $(this).siblings('.faq__desc').slideToggle();
//     $(this).toggleClass('active');
// })

$('input[type="tel"]').mask("+7-999-999-99-99");

$('.owl-partners').owlCarousel({
  loop: true,
  margin: 32,
  nav: true,
  responsive: {
    0: {
      items: 1
    },
    576: {
      items: 2
    },
    768: {
      items: 3
    },
    1000: {
      items: 4
    }
  }
});

var slider = $("#amount-one");
var output = $("#demo");
var $res = $("#res");
function calc() {
  var sliderVal = slider.val();
  var month = $(".term-investment__button.active").data("value");
  var payment = $("#result");
  output.val(sliderVal);
  var rate = 42.72;
  var k = rate / 12 / 100;

  var result = Math.floor(sliderVal * (k * (Math.pow(1 + k, month))) / ((Math.pow(1 + k, month)) - 1));
  payment.html(result.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
  $res.val(result);
}
calc();

slider.on('input', function () {
  calc();
});

output.change(function () {
  var val = parseInt($(this).val());
  slider.val(val);
  calc();
});
var $month = $("#demo2");


$('.first-buttons').click(function () {
  $('.first-buttons').removeClass("active");
  $(this).addClass("active");
  if ($(this).data("value") == '24') {
    rate = 42.72;
    $month.val("24");
  } else if ($(this).data("value") == '36') {
    rate = 51.1;
    $month.val("36");
  } else {
    rate = 62.26;
    $month.val("48");
  }
  calc();
});

var $range = $("#amount-one");
var $input = $("#demo");

var instance;
var min = 80000;
var max = 1000000;

$range.ionRangeSlider({
  skin: "round",
  type: "single",
  min: min,
  max: max,
  from: 80000,
  step: 10000,
  onStart: function (data) {
    $input.prop("value", data.from);
  },
  onChange: function (data) {
    $input.prop("value", data.from);
  }
});

instance = $range.data("ionRangeSlider");

$input.on("input", function () {
  var val = $(this).prop("value");
  // validate
  if (val < min) {
    val = min;
  } else if (val > max) {
    val = max;
  }
  instance.update({
    from: val
  });
});


//Второй калькулятор

var slider2 = $("#amount-two");
var output2 = $("#demo-two");
var $res2 = $("#res-two");
function calc2() {
  var sliderVal = slider2.val();
  var month = $(".term-investment__button.active.second").data("value");
  var payment2 = $("#result-two");
  output.val(sliderVal);
  var rate = 42.72;
  var k = rate / 12 / 100;
  var procentage = 0.05;

  /* var result = Math.floor(sliderVal / month + sliderVal * procentage); */
  var result = Math.floor(sliderVal * procentage);
  payment2.html(result.toString().replace(/\B(?=(\d{3})+(?!\d))/g, " "));
  $res2.val(result);
}
calc2();

slider2.on('input', function () {
  calc2();
});

output2.change(function () {
  var val = parseInt($(this).val());
  slider2.val(val);
  calc2();
});

var $month2 = $("#demo2-two");

$('.second').click(function () {
  $('.second').removeClass("active");
  $(this).addClass("active");
  if ($(this).data("value") == '3') {
    rate = 42.72;
    $month2.val("3");
  } else if ($(this).data("value") == '6') {
    rate = 51.1;
    $month2.val("6");
  } else {
    rate = 62.26;
    $month2.val("9");
  }
  calc2();
});

var $range = $("#amount-two");
var $input = $("#demo-two");

var instance;
var min = 50000;
var max = 1000000;

$range.ionRangeSlider({
  skin: "round",
  type: "single",
  min: min,
  max: max,
  from: 50000,
  step: 10000,
  onStart: function (data) {
    $input.prop("value", data.from);
  },
  onChange: function (data) {
    $input.prop("value", data.from);
  }
});

instance = $range.data("ionRangeSlider");

$input.on("input", function () {
  var val = $(this).prop("value");
  // validate
  if (val < min) {
    val = min;
  } else if (val > max) {
    val = max;
  }
  instance.update({
    from: val
  });
});

if ($('button.faq-opening-block-button').length) {

  var acc = document.getElementsByClassName("faq-opening-block-button");
  var i;

  for (i = 0; i < acc.length; i++) {
      acc[i].addEventListener("click", function () {
          this.classList.toggle("active");
          var panel = this.nextElementSibling;
          if (panel.style.maxHeight) {
              panel.style.maxHeight = null;
          } else {
              panel.style.maxHeight = panel.scrollHeight + "px";
          }
      });
  }
}